var joi = require('joi');

//category validator schema
var schema = joi.object().keys({
  params: joi.object().keys({
    appid: joi.number().integer(),
    filters: joi.object().keys({
      start: joi.date().required(),
      end: joi.date().min(joi.ref('start')).required(),
      status: joi.array().items(joi.string().valid(['live', 'ended'])).single(),
      location: joi.array().items(joi.number()).single(),
      campaigntype: joi.array().items(joi.string().valid(['promotion', 'reward'])).single(),
      merchant: joi.array().items(joi.number()).single(),
      category: joi.array().items(joi.number()).single(),
      campaign: joi.array().items(joi.number()).single()
    }),
    limit: joi.number(),
    fieldnames: joi.array().items(joi.string().valid(["categoryid", "categoryname", "categorycampaign", "categorycampaigncount", "campaignsharesmscount", "campaignsharefbcount", "campaignshareemailcount", "campaignsharewhatsappcount", "campaignsharetwittercount", "campaignsharewechatcount", "campaignalertcount", "campaignbrowsecount", "campaignalertsentcount", "campaignalertreceivedcount", "campaignalertclickedcount", "campaignpaymentfailedcount", "campaigncalendaraddedcount", "campaignclickedlinkcount", "campaigndownloadedcount", "campaignredeemedcount", "campaignsalesvalue"])),
    orderbyfield: joi.string().valid(joi.ref('fieldnames')),
    orderbytype: joi.string().valid(['asc', 'desc']),
    locale: joi.string().valid(['en_US', 'vi_VN', 'in_ID']).required()
  }).required()
}).required();

exports.validate = function(payload, callback) {
  payload = payload || {};
  joi.validate(payload, schema, {
    abortEarly: false,
    convert: true,
    stripUnknown: false
  }, function(err, value) {
    if (err) return callback(err);
    callback(null, value.params);
  });
}
