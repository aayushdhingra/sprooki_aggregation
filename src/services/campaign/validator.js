var joi = require('joi');

//campaign validator schema
var schema = joi.object().keys({
  params: joi.object().keys({
    appid: joi.number().integer(),
    filters: joi.object().keys({
      start: joi.date().required(),
      end: joi.date().min(joi.ref('start')).required(),
      location: joi.array().items(joi.number()).single(),
      campaigntype: joi.array().items(joi.string().valid(['promotion', 'reward'])).single(),
      merchant: joi.array().items(joi.number()).single(),
      category: joi.array().items(joi.number()).single(),
      campaign: joi.array().items(joi.number()).single()
    }),
    limit: joi.number().allow(null),
    fieldnames: joi.array().items(joi.string().valid(["campaignid", "campaignmerchantid", "campaignstatus", "campaignname", "campaigntype", "campaignsubtype", "campaigntnt", "campaigncategory", "campaignoutlet", "campaignlocation", "campaignvalue", "campaignretailvalue", "campaignsharesmscount", "campaignsharefbcount", "campaignshareemailcount", "campaignsharewhatsappcount", "campaignsharetwittercount", "campaignsharewechatcount", "campaignalertcount", "campaignbrowsecount", "campaignalertsentcount", "campaignalertreceivedcount", "campaignalertclickedcount", "campaignpaymentfailedcount", "campaigncalendaraddedcount", "campaignclickedlinkcount", "campaigndownloadedcount", "campaignredeemedcount", "campaignsalesvalue"])).min(1),
    orderbyfield: joi.string().valid(joi.ref('fieldnames')).allow(null),
    orderbytype: joi.string().valid(['asc', 'desc']).allow(null),
    locale: joi.string().valid(['en_US', 'vi_VN', 'in_ID']).required()
  }).required()
}).required();

exports.validate = function(payload, callback) {
  payload = payload || {};
  joi.validate(payload, schema, {
    abortEarly: false,
    convert: true,
    stripUnknown: false
  }, function(err, value) {
    if (err) return callback(err);
    callback(null, value.params);
  });
}
