'use strict';

//category handler
var moment = require('moment');
var async = require('async');
var mysql = require('mysql');
var includes = require('lodash/includes');
var intersection = require('lodash/intersection');
var validator = require('./validator');
var response = require('./response');


function buildQuery(params, connection, callback) {
  var querySelect = [];
  var queryFrom = [" `Categories` cat"];
  var queryWhere = [" cat.`Status`=0"];
  var queryGroupby = " GROUP BY cat.`CategoryId`";
  var queryOrderby = "";
  var queryLimit = "";

  var categoryFields = ["categoryid", "categoryname", "categorycampaign", "categorycampaigncount"];
  var analyticsFields = ["campaignsharesmscount", "campaignsharefbcount", "campaignshareemailcount", "campaignsharewhatsappcount", "campaignsharetwittercount", "campaignsharewechatcount", "campaignalertcount", "campaignbrowsecount", "campaignalertsentcount", "campaignalertreceivedcount", "campaignalertclickedcount", "campaignpaymentfailedcount", "campaigncalendaraddedcount", "campaignclickedlinkcount"];
  var transactionsFields = ["campaigndownloadedcount", "campaignredeemedcount", "campaignsalesvalue"];
  var analyticsMap = {
    "campaignsharesmscount": "10",
    "campaignsharefbcount": "11",
    "campaignshareemailcount": "12",
    "campaignsharewhatsappcount": "13",
    "campaignsharetwittercount": "14",
    "campaignsharewechatcount": "15",
    "campaignalertcount": "20",
    "campaignbrowsecount": "21",
    "campaignalertsentcount": "22",
    "campaignalertreceivedcount": "23",
    "campaignalertclickedcount": "24",
    "campaignpaymentfailedcount": "30",
    "campaigncalendaraddedcount": "40",
    "campaignclickedlinkcount": "41"
  };

  if (includes(params.fieldnames, "categoryid")) {
    querySelect.push(" cat.`CategoryId` AS `categoryid`");
  }
  if (includes(params.fieldnames, "categoryname")) {
    querySelect.push(" (SELECT `details` FROM Translation WHERE `columnType`=1 AND `objectType`=18 AND `language`='en_US' AND `objectId`=cat.`CategoryId` ) AS `categoryname`");
  }

  var intersectCategoryFields = intersection(params.fieldnames, categoryFields);
  var intersectAnalyticsFields = intersection(params.fieldnames, analyticsFields);
  var intersectTransactionFields = intersection(params.fieldnames, transactionsFields);

  //campaign join sub-query
  if(intersectCategoryFields.length > 0 || intersectAnalyticsFields.length > 0 || intersectTransactionFields.length > 0) {
    var queryCampaignSelect = [" cc.`CategoryId`"];
    var queryCampaignFrom = [" `CampaignCategory` cc INNER JOIN `Campaigns` c ON cc.`CampaignID`= c.`CampaignID`"];
    var queryCampaignWhere = [" c.`IsDeleted`= 0", " c.`status`<> 0"];
    var queryCampaignGroupby = " GROUP BY cc.`CategoryId`";

    //campaign subquery filters
    if (params.filters.start && params.filters.end) {
      queryCampaignWhere.push(" NOT(c.`EndTime` < '" + moment(params.filters.start).format('YYYY-MM-DD HH:mm:ss') + "' OR c.`StartTime` > '" + moment(params.filters.end).format('YYYY-MM-DD HH:mm:ss') + "')");
    }

    if ("campaigntype" in params.filters && params.filters.campaigntype.length > 0) {
      var temp = [];
      if(includes(params.filters.campaigntype, "promotion")) {
        temp.push(1);
      }
      if(includes(params.filters.campaigntype, "reward")) {
        temp.push(2);
      }
      queryCampaignWhere.push(" c.`Type` IN (" + temp.join(",") + ")");
    }

    if ("merchant" in params.filters && params.filters.merchant.length > 0) {
      queryCampaignWhere.push(" c.`MerchantID` IN (" + params.filters.merchant.join(",") + ")");
    }

    if ("category" in params.filters && params.filters.category.length > 0) {
      queryCampaignWhere.push(" cc.`CategoryID` IN (" + params.filters.category.join(",") + ")");
    }

    if ("campaign" in params.filters && params.filters.campaign > 0) {
      queryCampaignWhere.push(" c.`CampaignID` IN (" + params.filters.campaign.join(",") + ")");
    }

    if ("location" in params.filters && params.filters.location.length > 0) {
      queryCampaignFrom.push("LEFT JOIN (SELECT o.`OutletID`, co.`CampaignID`, o.`LocationId` FROM `CampaignOutlet` co INNER JOIN `Outlets` o USING (`OutletID`) GROUP BY co.`CampaignID`, co.`OutletID`) o ON o.`CampaignID`= c.`CampaignID`");
      queryCampaignWhere.push(" o.`locationId` IN (" + params.filters.location.join(",") + ")");
    }

    //campaign subquery select
    if (includes(params.fieldnames, "categorycampaign")) {
      querySelect.push(" c.`categorycampaign`");
      queryCampaignSelect.push(" GROUP_CONCAT(DISTINCT(cc.`CampaignID`)) AS `categorycampaign`");
    }

    if (includes(params.fieldnames, "categorycampaigncount")) {
      querySelect.push(" IFNULL(c.`categorycampaigncount`,0) AS `categorycampaigncount`");
      queryCampaignSelect.push(" COUNT(DISTINCT(cc.`CampaignID`)) AS `categorycampaigncount`");
    }

    // analytics
    if (intersectAnalyticsFields.length > 0) {
      var analyticsQuery = "LEFT JOIN (SELECT `campaignId`";
      for (var i = 0; i < intersectAnalyticsFields.length; i++) {
        querySelect.push(" IFNULL(c.`" + intersectAnalyticsFields[i] + "`,0) AS `" + intersectAnalyticsFields[i] + "`");
        queryCampaignSelect.push(" SUM(a.`" + intersectAnalyticsFields[i] + "`) AS `" + intersectAnalyticsFields[i] + "`");
        analyticsQuery += ", SUM(`" + analyticsMap[intersectAnalyticsFields[i]] + "`) AS `" + intersectAnalyticsFields[i] + "`";
      }
      analyticsQuery += " FROM `AggregatedCampaignAnalytics` WHERE `timestamp` BETWEEN " + moment(params.filters.start).utcOffset(params.timezone).format('X') + " AND " + moment(params.filters.end).utcOffset(params.timezone).format('X') + " GROUP BY `campaignId`) a ON a.`campaignId`=c.`CampaignID`";
      queryCampaignFrom.push(analyticsQuery);
    }

    // transactions
    if (intersectTransactionFields.length > 0) {
      if (includes(params.fieldnames, "campaigndownloadedcount")) {
        querySelect.push(" IFNULL(c.`campaigndownloadedcount`,0) AS `campaigndownloadedcount`");
        queryCampaignSelect.push(" SUM(t.`campaigndownloadedcount`) AS `campaigndownloadedcount`");
      }
      if (includes(params.fieldnames, "campaignredeemedcount")) {
        querySelect.push(" IFNULL(c.`campaignredeemedcount`,0) AS `campaignredeemedcount`");
        queryCampaignSelect.push(" SUM(t.`campaignredeemedcount`) AS `campaignredeemedcount`");
      }
      if (includes(params.fieldnames, "campaignsalesvalue")) {
        querySelect.push(" IFNULL(c.`campaignsalesvalue`,0) AS `campaignsalesvalue`");
        queryCampaignSelect.push(" SUM(IF(c.`IsTransactional`IN (1,3),(c.`CampaignValue`*t.`campaigndownloadedcount`),0)) AS `campaignsalesvalue`");
      }
      queryCampaignFrom.push("LEFT JOIN (SELECT `CampaignID`, COUNT(IF(`TransactionType` IN (0,3,5,6,7),1,NULL)) AS `campaigndownloadedcount`, COUNT(IF(`TransactionType`=1,1,NULL)) AS `campaignredeemedcount` FROM `Transactions` WHERE `Timestamp` BETWEEN '"+ moment(params.filters.start).format('YYYY-MM-DD HH:mm:ss') + "' AND '" + moment(params.filters.end).format('YYYY-MM-DD HH:mm:ss') + "' GROUP BY `CampaignID`) t ON t.`CampaignID`=c.`CampaignID`");
    }

    var campaignQuery = "SELECT" + queryCampaignSelect.join(",") + " FROM " + queryCampaignFrom.join(" ") + " WHERE" + queryCampaignWhere.join(" AND ") + queryCampaignGroupby;
    queryFrom.push("LEFT JOIN (" + campaignQuery + ") c ON c.`CategoryID`= cat.`CategoryId`");
  }

  if ("category" in params.filters && params.filters.category.length > 0) {
    queryWhere.push(" cat.`CategoryId` IN (" + params.filters.category.join(",") + ")");
  }

  if (params.orderbyfield) {
    queryOrderby += " ORDER BY `" + params.orderbyfield + "`";
    if (params.orderbytype) {
      queryOrderby += " " + params.orderbytype;
    }
  }

  if (params.limit) {
    queryLimit += " LIMIT " + params.limit;
  }

  var query = "SELECT" + querySelect.join(",") + " FROM " + queryFrom.join(" ") + " WHERE" + queryWhere.join(" AND ") + queryGroupby + queryOrderby + queryLimit;

  callback(null, query, params, connection);
}

function getData(query, params, connection, callback) {
  connection.changeUser({database: params.db});
  connection.query(query, [], function(err, result) {
    if (err) return callback(err);
    callback(null, result);
  });
}

function getApp(connection, params, callback) {
  var query = "SELECT m.value AS `db`, (SELECT `Value` FROM `ApplicationConfig` WHERE `Key`='TIMEZONE' AND `ApplicationId` IN ( ?, 0) ORDER BY `ApplicationId` DESC LIMIT 1 ) AS `timezone`, m.`entityid` AS `entityId`, a.`id` AS `appId` FROM `metadata` m INNER JOIN `applications` a ON a.`entityid`=m.`entityid` WHERE m.`name`='DB_DATABASE' AND a.`id`=?";
  connection.query(query, [params.appid, params.appid], function(err, result) {
    if (err) return callback(err);

    if (result.length > 0) {
      params["db"] = result[0].db;
      params["timezone"] = result[0].timezone;
      callback(null, params, connection);
    } else {
      callback('Invalid App Id: ' + params.appid);
    }
  });
}

module.exports.run = (event, context, runCallback) => {
  var data = JSON.parse(event.body), responseObj;
  var mysqlConnection = mysql.createConnection({
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DB
  });

  async.waterfall([
    async.apply(validator.validate, data),
    async.apply(getApp, mysqlConnection),
    buildQuery,
    getData,
  ], function(err, result) {
    mysqlConnection.end();
    if(err) {
      if(err.isJoi) {
        responseObj = response.badRequest(err.message);
      } else {
        responseObj = response.internalServerError(err);
      }
    } else {
      responseObj = response.success(result);
    }
    runCallback(null, responseObj);
  });
};
