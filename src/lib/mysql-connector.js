var mysql = require('mysql');

var state = {
  pool: null,
}

var info = {
  connectionLimit: 4,
  host: process.env.HOST,
  user: process.env.USER,
  password: process.env.PASSWORD,
  port: 3306,
  database: process.env.DB
};

exports.connect = function(done) {
  state.pool = mysql.createPool(info);
  done();
}

exports.get = function() {
  return state.pool;
}

exports.end = function() {
  if (state.pool) {
    state.pool.end();
  }
}

// exports.changeDb = function(db) {
//   if (state.pool) {
//     state.pool.changeUser({database: db});
//   }
// }

exports.changeDb = function(db, done) {
  if (state.pool) {
    state.pool.end();
  }
  var newInfo = info;
  newInfo.database = db;
  state.pool = mysql.createPool(newInfo);
  done();
}
