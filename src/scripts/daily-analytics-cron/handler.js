'use strict';

var moment = require('moment');
var async = require('async');
var mysqldb = require('../../lib/mysql-connector');

function log(message) {
  return console.log('[' + moment().format('YYYY-MM-DD HH:mm:ss') + '] ' + message);
}

function updateLastSyncTime(config, callback) {
  var query = "UPDATE `metadata` SET `value`=? WHERE `name`='AGGREGATION:ANALYTICS_LAST_SYNC_TIME' AND `entityid`=? ";
  var values = [config.end, config.entityId];
  mysqldb.get().query(query, values, function(err, result) {
    if (err) return callback(err);
    callback(null, result);
  });
}

function storeAggregatedCampaignAnalytics(rows, config, callback) {
  var query = "INSERT INTO `AggregatedCampaignAnalytics` (`campaignId`, `appId`, `timestamp`, `10`, `11`, `12`, `13`, `14`, `15`, `20`, `21`, `22`, `23`, `24`, `30`, `40`, `41`) VALUES ? ",
    i, j, insertRows = [];
  query += "ON DUPLICATE KEY UPDATE `10`=VALUES(`10`), `11`=VALUES(`11`), `12`=VALUES(`12`), `13`=VALUES(`13`), `14`=VALUES(`14`), `15`=VALUES(`15`), `20`=VALUES(`20`), `21`=VALUES(`21`), `22`=VALUES(`22`), `23`=VALUES(`23`), `24`=VALUES(`24`), `30`=VALUES(`30`), `40`=VALUES(`40`), `41`=VALUES(`41`)";
  for (j = 0; j < rows.length; j++) {
    if (rows[j].length > 0) {
      for (i = 0; i < rows[j].length; i++) {
        insertRows.push([
          rows[j][i].campaignId, config.appId, rows[j][i].timestamp, rows[j][i]['10'], rows[j][i]['11'], rows[j][i]['12'], rows[j][i]['13'], rows[j][i]['14'], rows[j][i]['15'], rows[j][i]['20'], rows[j][i]['21'], rows[j][i]['22'], rows[j][i]['23'], rows[j][i]['24'], rows[j][i]['30'], rows[j][i]['40'], rows[j][i]['41']
        ]);
      }
    }
  }
  if (insertRows.length > 0) {
    mysqldb.get().query(query, [insertRows], function(err, result) {
      if (err) return callback(err);
      callback(null, insertRows.length);
    });
  } else {
    callback(null, 0);
  }
}

function getAggregationByTime(config, start, callback) {
  var start = moment(start);
  var end = moment(start).endOf(config.aggregationIntervalUnit);
  var query = "SELECT a.ObjectId AS `campaignId`,UNIX_TIMESTAMP(?) AS `timestamp`,COUNT(IF(a.Actiontype=10,1,NULL)) AS `10`,COUNT(IF(a.Actiontype=11,1,NULL)) AS `11`,COUNT(IF(a.Actiontype = 12, 1, NULL)) AS `12`, COUNT(IF(a.Actiontype = 13, 1, NULL)) AS `13`, COUNT(IF(a.Actiontype = 14, 1, NULL)) AS `14`, COUNT(IF(a.Actiontype = 15, 1, NULL)) AS `15`, COUNT(IF(a.Actiontype = 20, 1, NULL)) AS `20`, COUNT(IF(a.Actiontype = 21, 1, NULL)) AS `21`, COUNT(IF(a.Actiontype = 22, 1, NULL)) AS `22`, COUNT(IF(a.Actiontype = 23, 1, NULL)) AS `23`, COUNT(IF(a.Actiontype = 24, 1, NULL)) AS `24`, COUNT(IF(a.Actiontype = 30, 1, NULL)) AS `30`, COUNT(IF(a.Actiontype = 40, 1, NULL)) AS `40`, COUNT(IF(a.Actiontype = 41, 1, NULL)) AS `41` FROM Analytics a WHERE a.ObjectType = 1 AND a.Timestamp BETWEEN ? AND ? GROUP BY a.ObjectId ";
  var values = [start.format('YYYY-MM-DD HH:mm:ss'), start.format('YYYY-MM-DD HH:mm:ss'), end.format('YYYY-MM-DD HH:mm:ss')];

  mysqldb.get().query(query, values, function(err, result) {
    if (err) return callback(err);
    callback(null, result);
  });
}

function getAggregationByDates(dates, config, callback) {
  if (dates.length > 0) {
    async.mapLimit(dates, config.parallelWorkers, getAggregationByTime.bind(getAggregationByTime, config), function(err, results) {
      callback(null, results, config);
    });
  } else {
    callback(null, [], config);
  }
}

function startAggregationByApp(config, callback) {
  var end = moment(config.end),
    batchStart = moment(config.start).utcOffset(config.utcOffset),
    count = 0;
  var returnResult = {
    'app': config.appId,
    'result': 'success',
    'error': null,
    'lastSyncTime': config.end,
    'insertedCount': 0,
  };

  if (batchStart.isSame(end)) {
    returnResult.result = 'fail';
    returnResult.error = 'same start and end time';
    return callback(null, returnResult);
  }
  log('Aggregating for App ID: ' + config.appId + ' (' + batchStart.format('YYYY-MM-DD HH:mm:ss') + ' - ' + end.format('YYYY-MM-DD HH:mm:ss') + ')');

  async.waterfall([
      //change to client db
      function(callback) {
        mysqldb.changeDb(config.db, function(err) {
          if (err) return callback(err);
          callback();
        });
      },
      //aggregate for app
      function(callback) {
        async.whilst(() => batchStart.isBefore(end),
          function(next) {
            async.waterfall([
              function(callback) {
                var dates = [],
                  dateCounter = 0;
                while (batchStart.isBefore(end) && dateCounter < config.insertBatchSize) {
                  dates.push(batchStart.format('YYYY-MM-DD HH:mm:ss'));
                  batchStart.add(config.aggregationIntervalValue, config.aggregationIntervalUnit);
                  dateCounter++;
                }
                callback(null, dates, config)
              },
              getAggregationByDates,
              storeAggregatedCampaignAnalytics,
            ], function(err, storedCount) {
              if (err) return next(err);
              count += storedCount;
              next(null, count);
            });
          },
          function(err, count) {
            if (err) return callback(err);
            log('Total records inserted: ' + count);
            callback(null, count);
          }
        );
      },
      //change db to metadata
      function(insertedCount, callback) {
        mysqldb.changeDb(process.env.DB, function(err) {
          if (err) return callback(err);
          callback(null, insertedCount);
        });
      },
    ],
    //update last sync time
    function(err, insertedCount) {
      if (err) {
        returnResult.result = 'fail';
        returnResult.error = err;
        returnResult.lastSyncTime = config.start;
        callback(null, returnResult);
      } else {
        updateLastSyncTime(config, function(err, result) {
          if (err) return callback(err);
          log('Last sync time updated to ' + config.end);
          returnResult.insertedCount = insertedCount;
          callback(null, returnResult);
        });
      }
    });
}

function aggregateApps(apps, callback) {
  if (apps.length > 0) {
    async.mapSeries(apps, startAggregationByApp, function(err, result) {
      if (err) return callback(err);
      callback(null, result);
    });
  } else {
    callback('no apps found for aggregation');
  }
}

function processAppsForAggregation(rawApps, callback) {
  if (rawApps.length > 0) {
    var apps = [],
      i, appConfig;
    for (i = 0; i < rawApps.length; i++) {
      if (!rawApps[i].config) {
        log('Notice: Using default config for app ID ' + rawApps[i].appId);
        appConfig = {
          "insertBatchSize": 24,
          "aggregationIntervalValue": 1,
          "aggregationIntervalUnit": "hour",
          "parallelWorkers": 12,
          "utcOffset": "+0800"
        };
      } else {
        appConfig = JSON.parse(rawApps[i].config);
      }
      apps.push({
        "insertBatchSize": appConfig.insertBatchSize,
        "aggregationIntervalValue": appConfig.aggregationIntervalValue,
        "aggregationIntervalUnit": appConfig.aggregationIntervalUnit,
        "parallelWorkers": appConfig.parallelWorkers,
        "utcOffset": appConfig.utcOffset,
        "appId": rawApps[i].appId,
        "entityId": rawApps[i].entityId,
        "db": rawApps[i].db,
        "start": rawApps[i].syncTime,
        "end": moment().utcOffset(appConfig.utcOffset).startOf('day').format('YYYY-MM-DD HH:mm:ss')
      });
    }
    callback(null, apps);
  } else {
    callback('no apps enabled for aggregation');
  }
}

function getAppsForAggregation(callback) {
  var query = "SELECT m.value AS `db`, m.`entityid` AS `entityId`, (SELECT `id` FROM `applications` a WHERE a.`entityid`=m.`entityid`) AS `appId`, (SELECT `value` FROM `metadata` WHERE `entityid`=m.`entityid` AND `name`='AGGREGATION:ANALYTICS_LAST_SYNC_TIME') AS `syncTime`, (SELECT `value` FROM `metadata` WHERE `entityid` IN (m.`entityid`,0) AND `name`='AGGREGATION:CONFIG' ORDER BY entityid DESC LIMIT 1) AS `config` FROM `metadata` m WHERE m.`name`='DB_DATABASE' AND m.`entityid` IN (SELECT `entityid` FROM `metadata` WHERE `value`=1 AND `name`='AGGREGATION:ANALYTICS')";
  mysqldb.get().query(query, function(err, result) {
    if (err) return callback(err);
    callback(null, result);
  });
}

module.exports.run = (event, context, runCallback) => {
  async.waterfall([
    function(callback) {
      mysqldb.connect(function(err) {
        if (err) return runCallback(err);
        callback();
      });
    },
    getAppsForAggregation,
    processAppsForAggregation,
    aggregateApps,
  ], function(err, result) {
    mysqldb.end();
    var finalResult = {
      'result': 'success',
      'appResults': result
    };
    if (err) {
      finalResult.result = 'fail';
    }
    runCallback(err, finalResult);
  });
};
