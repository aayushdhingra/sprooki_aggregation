'use strict';

var moment = require('moment');
var async = require('async');
var mysql = require('mysql');
var includes = require('lodash/includes');
var intersection = require('lodash/intersection');
var validator = require('./validator');
var response = require('./response');


function buildQuery(params, connection, callback) {
  var querySelect = [];
  var queryFrom = ["`Campaigns` c"];
  var queryWhere = [" c.`IsDeleted`= 0", " c.`status`<> 0"];
  var queryGroupby = " GROUP BY c.`CampaignID`";
  var queryOrderby = "";
  var queryLimit = "";

  var campaignFields = ["campaign", "campaignmerchantid", "campaigntype", "campaignsubtype", "campaignvalue", "campaignretailvalue", "campaignstatus", "campaignname"];
  var analyticsFields = ["campaignsharesmscount", "campaignsharefbcount", "campaignshareemailcount", "campaignsharewhatsappcount", "campaignsharetwittercount", "campaignsharewechatcount", "campaignalertcount", "campaignbrowsecount", "campaignalertsentcount", "campaignalertreceivedcount", "campaignalertclickedcount", "campaignpaymentfailedcount", "campaigncalendaraddedcount", "campaignclickedlinkcount"];
  var transactionsFields = ["campaigndownloadedcount", "campaignredeemedcount", "campaignsalesvalue"];
  var analyticsMap = {
    "campaignsharesmscount": "10",
    "campaignsharefbcount": "11",
    "campaignshareemailcount": "12",
    "campaignsharewhatsappcount": "13",
    "campaignsharetwittercount": "14",
    "campaignsharewechatcount": "15",
    "campaignalertcount": "20",
    "campaignbrowsecount": "21",
    "campaignalertsentcount": "22",
    "campaignalertreceivedcount": "23",
    "campaignalertclickedcount": "24",
    "campaignpaymentfailedcount": "30",
    "campaigncalendaraddedcount": "40",
    "campaignclickedlinkcount": "41"
  };

  if (includes(params.fieldnames, "campaignid")) {
    querySelect.push(" c.`CampaignID` AS `campaignid`");
  }
  if (includes(params.fieldnames, "campaignmerchantid")) {
    querySelect.push(" c.`MerchantID` AS `campaignmerchantid`");
  }
  if (includes(params.fieldnames, "campaigntype")) {
    querySelect.push(" c.`Type` AS `campaigntype`");
  }
  if (includes(params.fieldnames, "campaignsubtype")) {
    querySelect.push(" c.`IsTransactional` AS `campaignsubtype`");
  }
  if (includes(params.fieldnames, "campaignstatus")) {
    querySelect.push(" IF((c.`status` = 1 OR c.`EndTime` < NOW()), 'ended', 'live') AS `campaignstatus`");
  }
  if (includes(params.fieldnames, "campaigntnt")) {
    querySelect.push(" IF(c.`IsTransactional`IN (1,3), 'T', 'NT') AS `campaigntnt`");
  }
  if (includes(params.fieldnames, "campaignname")) {
    querySelect.push(" (SELECT `details` FROM Translation WHERE `columnType`=1 AND `objectType`=5 AND `language`='" + params.locale + "' AND `objectId`=c.`CampaignID`) AS `campaignname`");
  }
  if (includes(params.fieldnames, "campaigncategory")) {
    querySelect.push(" (SELECT GROUP_CONCAT(`CategoryID`) FROM `CampaignCategory` WHERE `CampaignID`=c.`CampaignID`) AS `campaigncategory`");
  }
  if (includes(params.fieldnames, "campaignoutlet")) {
    querySelect.push(" (SELECT GROUP_CONCAT(`OutletID`) FROM `CampaignOutlet` WHERE `CampaignID`=c.`CampaignID` ) AS `campaignoutlet`");
  }
  if (includes(params.fieldnames, "campaignlocation")) {
    querySelect.push(" (SELECT GROUP_CONCAT(`LocationId`) FROM `Outlets` INNER JOIN `CampaignOutlet` USING(`OutletID`) WHERE `CampaignID`=c.`CampaignID`) AS `campaignlocation`");
  }
  if (includes(params.fieldnames, "campaignretailvalue")) {
    querySelect.push(" c.`CampaignValue` AS `campaignvalue`");
  }
  if (includes(params.fieldnames, "campaignvalue")) {
    querySelect.push(" c.`RetailValue` AS `campaignretailvalue`");
  }

  if (params.filters.start && params.filters.end) {
    queryWhere.push(" NOT(c.EndTime < '" + moment(params.filters.start).format('YYYY-MM-DD HH:mm:ss') + "' OR c.StartTime > '" + moment(params.filters.end).format('YYYY-MM-DD HH:mm:ss') + "')");
  }

  if ("campaigntype" in params.filters && params.filters.campaigntype.length > 0) {
    var temp = [];
    if(includes(params.filters.campaigntype, "promotion")) {
      temp.push(1);
    }
    if(includes(params.filters.campaigntype, "reward")) {
      temp.push(2);
    }
    queryWhere.push(" c.`Type` IN (" + temp.join(",") + ")");
  }

  if ("merchant" in params.filters && params.filters.merchant.length > 0) {
    queryWhere.push(" c.`MerchantID` IN (" + params.filters.merchant.join(",") + ")");
  }

  if ("category" in params.filters && params.filters.category.length > 0) {
    queryFrom.push("LEFT JOIN `CampaignCategory` cc ON cc.`CampaignID`= c.`CampaignID`");
    queryWhere.push(" cc.`CategoryID` IN (" + params.filters.category.join(",") + ")");
  }

  if ("campaign" in params.filters && params.filters.campaign > 0) {
    queryWhere.push(" c.`CampaignID` IN (" + params.filters.campaign.join(",") + ")");
  }

  if ("location" in params.filters && params.filters.location.length > 0) {
    queryFrom.push("LEFT JOIN (SELECT o.`OutletID`, co.`CampaignID`, o.`LocationId` FROM `CampaignOutlet` co INNER JOIN `Outlets` o USING (`OutletID`) GROUP BY co.`CampaignID`, co.`OutletID`) o ON o.`CampaignID`= c.`CampaignID`");
    queryWhere.push(" o.`locationId` IN (" + params.filters.location.join(",") + ")");
  }

  // analytics
  var intersectFields = intersection(params.fieldnames, analyticsFields);
  if (intersectFields.length > 0) {
    var analyticsQuery = "LEFT JOIN (SELECT `campaignId`";
    for (var i = 0; i < intersectFields.length; i++) {
      querySelect.push(" IFNULL(a.`" + intersectFields[i] + "`,0) AS `" + intersectFields[i] + "`");
      analyticsQuery += ", SUM(`" + analyticsMap[intersectFields[i]] + "`) AS `" + intersectFields[i] + "`";
    }
    analyticsQuery += " FROM `AggregatedCampaignAnalytics` WHERE `timestamp` BETWEEN " + moment(params.filters.start).utcOffset(params.timezone).format('X') + " AND " + moment(params.filters.end).utcOffset(params.timezone).format('X') + " GROUP BY `campaignId`) a ON a.`campaignId`=c.`CampaignID`";
    queryFrom.push(analyticsQuery);
  }
  // transactions
  intersectFields = intersection(params.fieldnames, transactionsFields);
  if (intersectFields.length > 0) {
    if (includes(params.fieldnames, "campaigndownloadedcount")) {
      querySelect.push(" IFNULL(t.`campaigndownloadedcount`,0) AS `campaigndownloadedcount`");
    }
    if (includes(params.fieldnames, "campaignredeemedcount")) {
      querySelect.push(" IFNULL(t.`campaignredeemedcount`,0) AS `campaignredeemedcount`");
    }
    if (includes(params.fieldnames, "campaignsalesvalue")) {
      querySelect.push(" IFNULL(IF(c.`IsTransactional`IN (1,3),(c.`CampaignValue`*t.`campaigndownloadedcount`),0),0) AS `campaignsalesvalue`");
    }
    queryFrom.push("LEFT JOIN (SELECT `CampaignID`, COUNT(IF(`TransactionType` IN (0,3,5,6,7),1,NULL)) AS `campaigndownloadedcount`, COUNT(IF(`TransactionType`=1,1,NULL)) AS `campaignredeemedcount` FROM `Transactions` WHERE `Timestamp` BETWEEN '"+ moment(params.filters.start).format('YYYY-MM-DD HH:mm:ss') + "' AND '" + moment(params.filters.end).format('YYYY-MM-DD HH:mm:ss') + "' GROUP BY `CampaignID`) t ON t.`CampaignID`=c.`CampaignID`");
  }

  if (params.orderbyfield) {
    queryOrderby += " ORDER BY `" + params.orderbyfield + "`";
    if (params.orderbytype) {
      queryOrderby += " " + params.orderbytype;
    }
  }

  if (params.limit) {
    queryLimit += " LIMIT " + params.limit;
  }

  var query = "SELECT" + querySelect.join(",") + " FROM " + queryFrom.join(" ") + " WHERE" + queryWhere.join(" AND ") + queryGroupby + queryOrderby + queryLimit;

  callback(null, query, params, connection);
}

function getData(query, params, connection, callback) {
  connection.changeUser({database: params.db});
  connection.query(query, [], function(err, result) {
    if (err) return callback(err);
    callback(null, result);
  });
}

function getApp(connection, params, callback) {
  var query = "SELECT m.value AS `db`, (SELECT `Value` FROM `ApplicationConfig` WHERE `Key`='TIMEZONE' AND `ApplicationId` IN ( ?, 0) ORDER BY `ApplicationId` DESC LIMIT 1 ) AS `timezone`, m.`entityid` AS `entityId`, a.`id` AS `appId` FROM `metadata` m INNER JOIN `applications` a ON a.`entityid`=m.`entityid` WHERE m.`name`='DB_DATABASE' AND a.`id`=?";
  connection.query(query, [params.appid, params.appid], function(err, result) {
    if (err) return callback(err);

    if (result.length > 0) {
      params["db"] = result[0].db;
      params["timezone"] = result[0].timezone;
      callback(null, params, connection);
    } else {
      callback('Invalid App Id: ' + params.appid);
    }
  });
}

module.exports.run = (event, context, runCallback) => {
  var data = JSON.parse(event.body), responseObj;
  var mysqlConnection = mysql.createConnection({
    host: process.env.HOST,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DB
  });

  async.waterfall([
    async.apply(validator.validate, data),
    async.apply(getApp, mysqlConnection),
    buildQuery,
    getData,
  ], function(err, result) {
    mysqlConnection.end();
    if(err) {
      if(err.isJoi) {
        responseObj = response.badRequest(err.message);
      } else {
        responseObj = response.internalServerError(err);
      }
    } else {
      responseObj = response.success(result);
    }
    runCallback(null, responseObj);
  });
};
